function htmlCreate(args={}) {
    let what = args.what;
    delete args.what;
    
    if (what === undefined)
      what = "div";
    let d = document.createElement(what);
    
    if (args.appendTo) {
      args.appendTo.appendChild(d);
      delete args.appendTo;
    }
    for (const property in args) {
      if (d[property] === undefined) {
        if (d.style[property] === undefined) {
      throw `Unrecognized '${what}' property: ${property}`;
        }
        d.style[property] = args[property];
        continue;
      }
      d[property] = args[property];
    }
    return d;
}

const table = document.querySelector('#collection-table');

const modes = {
    EDITING: "EDITING",
    VIEWING: "VIEWING"
};

class Entry {
    constructor(entry) {
        this.entry = entry;
        this.mode = modes.EDITING;
    }
    setElements(args) {
        this.author = args.author;
        this.title = args.title;
        this.nav = args.nav;
        this.titleInput = args.titleInput;
        this.authorInput = args.authorInput;
        this.seButton = args.seButton;
        this.rmButton = args.rmButton;
    }
    toggleMode() {
        if (this.mode === modes.EDITING) {
            this.mode = modes.VIEWING;
            this.seButton.innerText = "Edit";
        } else {
            this.mode = modes.EDITING;
            this.seButton.innerText = "Save";
        }
        this.authorInput.disabled = !this.authorInput.disabled;
        this.titleInput.disabled = !this.titleInput.disabled;
        this.authorInput.classList.toggle("disabled");
        this.titleInput.classList.toggle("disabled");
    }
    removeSelf() {
        this.entry.remove();
    }
}

function addCallback() {
    let entryNode = htmlCreate({
        classList: "book",
        appendTo: table
    });
    let newEntry = new Entry(entryNode);

    let author = htmlCreate({
        classList: "author",
        appendTo: entryNode
    });
    let title = htmlCreate({
        classList: "title",
        appendTo: entryNode
    });
    let nav = htmlCreate({
        classList: "table-nav",
        appendTo: entryNode
    });
    let authorInput = htmlCreate({
        what: "input",
        type: "text",
        appendTo: author
    });
    let titleInput = htmlCreate({
        what: "input",
        type: "text",
        appendTo: title
    });
    let seButton = htmlCreate({
        what: "button",
        innerText: "Save",
        onclick: () => newEntry.toggleMode(),
        appendTo: nav
    });
    let rmButton = htmlCreate({
        what: "button",
        innerText: "Remove",
        onclick: () => newEntry.removeSelf(),
        appendTo: nav
    });

    newEntry.setElements({author: author, title: title, nav: nav,
        authorInput: authorInput, titleInput: titleInput, seButton: seButton, rmButton: rmButton});
}

document.querySelector("#add-button").addEventListener("click", addCallback);